/* global Materialize */
/*jshint esversion: 6 */

/**********************************************************
 C L A S S    D E F I N I T I O N S
 **********************************************************/

class MediaQueries {
    static medium() {
        var materializeMedium = window.matchMedia("only screen and (min-width: 601px)");
        return materializeMedium;
    }

    static large() {
        var materializeLarge = window.matchMedia("only screen and (min-width: 601px)");
        return materializeLarge;
    }
}

class UserMealService {

    static getUserMacro(callback) {
        var userMacroUrl = "/get-macro";
        UserMealService.__ajax(userMacroUrl, callback);
    }

    static getUserMeal(callback) {
        var userMealUrl = "/get-meal";
        UserMealService.__ajax(userMealUrl, callback);
    }

    static addToMeal(foodId, callback) {
        var addFoodUrl = "/add-to-meal";
        UserMealService.__ajax(addFoodUrl, callback, {foodId});
    }

    static removeFood(foodId, callback) {
        var removeFoodUrl = "/remove-from-meal";
        UserMealService.__ajax(removeFoodUrl, callback, {foodId});
    }

    static __ajax(url, callback, data) {
        $.post(url, data, (data, status) => {
            if (typeof callback === 'function') {
                callback(data, status);
            }
        });
    }
}




class RestaurantsService {

    /**
     * will return object representing categories and foods for given restaurant
     * @param {String} restaurantName
     * @param {Function} callback
     */
    static getMenu(restaurantName, callback) {
        let cachedMenu = RestaurantsService.restaurantMenu[restaurantName],
                restaurantUrl = `/list-foods?restaurant=${restaurantName}`;

        if (typeof callback !== 'function') {
            callback = false;
        }

        if (cachedMenu) {
            if (callback) {
                callback(cachedMenu);
            }
        } else {
            $.get(restaurantUrl, menu => {
                RestaurantsService.restaurantMenu[restaurantName] = menu;
                if (callback) {
                    callback(menu);
                }
            });
        }
    }

    /**
     * Will return foods for given restaurant and category
     * @param {type} restaurant
     * @param {type} categoryId
     * @param {type} callback
     * @returns {undefined}
     */
    static getFoods(restaurant, categoryId, callback) {
        let cachedMenu = RestaurantsService.restaurantMenu[restaurant];

        if (typeof callback !== 'function') {
            callback = false;
        }

        if (cachedMenu) {
            if (callback) {
                let foods = RestaurantsService.__findInMenu(cachedMenu, categoryId);
                callback(foods);
            }
        } else { //-- we need to first download menu
            RestaurantsService.getMenu(restaurant, menu => {
                let foods = RestaurantsService.__findInMenu(menu, categoryId);
                callback(foods);
            });
        }
    }

    static __findInMenu(menu, categoryId) {
        for (let property of Object.keys(menu)) {
            let category = Category.of(property);
            if (category.id === categoryId) {
                return menu[property];
            }
        }
    }
}
RestaurantsService.restaurantMenu = {}; //cache


/*
 * Will return objects representing different activities and duration in minutes
 * required to burn 100kcal.
 * @type type
 */
class ActivitiesService {

    static loadAndCacheActivities(callback) {
        var activitiesUrl = "/get-activities";
        $.getJSON(activitiesUrl, json => {
            ActivitiesService.activites = json;
            if (typeof callback === 'function') {
                callback(json);
            }
        });
    }

    static getActivites(callback) {
        if ($.isEmptyObject(ActivitiesService.activities)) {
            ActivitiesService.loadAndCacheActivities(callback);
        }
    }

    static getRandom() {
        var activities = ActivitiesService.activites,
                length = Object.keys(activities).length,
                randomNumber = Math.floor(Math.random() * length);

        return activities[randomNumber];
    }
}
ActivitiesService.activites = {}; //cache




class ActivityCalculator {
    constructor(nrOfElements, kcal) {
        this.nrOfElements = nrOfElements;
        this.kcal = kcal;
    }
    
    static getRandomItems(count, kcal){
        let calc = new ActivityCalculator(count, kcal);
        return calc.elements;
    }
    
    get elements() {
        let items = document.createDocumentFragment();

        for (let i = this.nrOfElements; i > 0; i--) {
            let li = document.createElement("li"),
                    activity = ActivitiesService.getRandom(),
                    duration = this._calculateBurningTime(this.kcal, activity);
            li.innerHTML = activity.name + " przez " + this._parseToHours(duration);
            items.appendChild(li);
        }
        
        return items;
    }

    _calculateBurningTime(kcal, activity) {
        var times = kcal / 100;
        return Math.round(activity.duration * times);
    }

    _parseToHours(minutes) {
        var h = Math.floor(minutes / 60),
                min = minutes % 60;

        return `${h}h i ${min}min.`;
    }
}





class Category {
    constructor(id, restaurant, name) {
        if (!(this instanceof Category)) {
            return new Category(id, restaurant, name);
        }

        this.id = id;
        this.restaurant = restaurant;
        this.name = name;
    }

    static of(string) {
        string = string.split(":");
        return new Category(string[0], string[1], string[2]);
    }
}




class RestaurantNavigation {
    constructor(list) {
        if (!(this instanceof RestaurantNavigation)) {
            return new RestaurantNavigation(list);
        }

        this._onClick = [];
        this.listName = list;

        let radioButtons = document.querySelectorAll(list + " > li > input[type=radio]"),
                setActive = e => {
                    $(list + ' > li.active').toggleClass('active');
                    $(e.target.parentNode).toggleClass('active');

                    for (let callback of this._onClick) {
                        callback(e.target, e);
                    }
                };

        for (let button of radioButtons) {
            button.addEventListener('click', setActive);
        }
    }

    selectedRestaurantName() {
        let selectedElement = document.querySelector(this.listName + " input[type=radio]:checked");
        return selectedElement.getAttribute('value');
    }

    addOnClickListener(callback) {
        if (typeof callback === 'function') {
            this._onClick.push(callback);
        }
    }
}




class ResultBar {
    constructor(resultBar, footer) {
        if (!(this instanceof ResultBar)) {
            return new ResultBar(resultBar, footer);
        }

        this.resultBar = document.querySelector(resultBar);
        this.footer = document.querySelector(footer);

        MediaQueries.medium().addListener(mq => {
            let display = this.resultBar.style.display;
            if (mq.matches && display !== 'none') {
                this.footer.style.marginBottom = "50px";
            } else {
                this.footer.style.marginBottom = "30px";
            }
        });
    }

    showResultBar() {
        this.resultBar.style.display = "inline-block";
        if (MediaQueries.medium().matches) {
            this.footer.style.marginBottom = "50px";
        } else {
            this.footer.style.marginBottom = "30px";
        }

    }

    hideResultBar() {
        this.resultBar.style.display = "none";
        this.footer.style.marginBottom = "0";
    }

    setUserMacro(userMacro) {
        var kcal = Math.round(userMacro.kcal),
                carbohydrates = Math.round(userMacro.carbohydrates),
                proteins = Math.round(userMacro.proteins),
                fats = Math.round(userMacro.proteins),
                desc = `Kcal: ${kcal}, W: ${carbohydrates}g, B: ${proteins}g, T: ${fats}g`;

        this.resultBar.querySelector("#result-txt").innerHTML = desc;

        if (userMacro.kcal === 0) {
            this.hideResultBar();
        }
    }
}


class Preloader {
    constructor(selector) {
        this.__preloader = $(selector);
    }

    show() {
        this.__preloader.css("width", "100%");
        this.__preloader.css("visibility", "visible");
    }

    hide() {
        this.__preloader.css("width", "0");
        this.__preloader.animate({
            width: "0"
        }, 100, () => this.__preloader.css("visibility", "hidden"));
    }
}


class CategoriesSelect {
    constructor(containerName) {
        this._container = $(containerName);
        this._select = this._container.find("select");
        this._onClick = [];

        this._select.change(event => {
            for (let callback of this._onClick) {
                callback(event.target.value);
            }
        });
    }

    fill(foodsMap, deleteOld) {
        if (deleteOld) {
            this.deleteOptions();
        }

        for (let category of Object.keys(foodsMap)) {
            this.__addOption(category);
        }

        if ($.isEmptyObject(foodsMap)) {
            this._container.addClass('empty');
        } else {
            this._container.removeClass('empty');
        }

        this._select.material_select();
    }

    disable() {
        this._select.attr("disabled");
    }

    enable() {
        this._select.removeAttr("disabled");
    }

    selectOption(nr) {
        if (this._select.children().length >= nr) {
            let value = this._select.find('option:eq(' + nr + ')').val();
            this._select.val(value).change();
            this._select.material_select();
        }
    }

    addOnClickListener(callback) {
        if (typeof callback === 'function') {
            this._onClick.push(callback);
        }
    }

    deleteOptions() {
        this._select.material_select('destroy');
        this._select.empty();
    }

    __addOption(categoryData) {
        var category = Category.of(categoryData),
                option = document.createElement("option"),
                text = document.createTextNode(category.name);

        option.value = category.id;
        option.appendChild(text);
        this._select.append(option);
    }
}




class UserMacro {
    constructor(kcal = 0, fats = 0, proteins = 0, carbohydrates = 0) {
        this.kcal = kcal;
        this.fats = fats;
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
    }

    add(food) {
        this.kcal += food.kcal;
        this.fats += food.fats;
        this.proteins += food.proteins;
        this.carbohydrates += food.carbohydrates;
    }

    remove(food) {
        this.kcal -= food.kcal;
        this.fats -= food.fats;
        this.proteins -= food.proteins;
        this.carbohydrates -= food.carbohydrates;
    }
}


class UserMealList {
    constructor(list) {
        if (!(this instanceof UserMealList)) {
            return new UserMealList();
        }

        this.list = document.querySelector(list);
        this._foodIndex = 0; //to keep count of user meal
        this._onDelete = [];
        this.macro = new UserMacro();
    }

    loadUserMeal(callback) {
        UserMealService.getUserMeal((foodList, status) => {
            if (foodList && status) {
                let listItems = document.createDocumentFragment();
                for (let simpleFood of foodList) {
                    let listItem = this.__createItem(simpleFood, ++this._foodIndex);
                    listItems.appendChild(listItem);
                    this.macro.add(simpleFood);
                }

                this.list.appendChild(listItems);

                if (typeof callback === 'function') {
                    callback(foodList);
                }
            }

            if (this.getFoodCount() < 1) {
                this.list.style.visibility = "hidden";
            } else {
                this.list.style.visibility = "visible";
            }
        });
    }

    addOnDeleteListener(callback) {
        if (typeof callback === 'function') {
            this._onDelete.push(callback);
        }
    }

    addToUserMeal(simpleFood) {
        var listItem = this.__createItem(simpleFood, ++this._foodIndex);

        this.list.appendChild(listItem);
        this.macro.add(simpleFood);
        this.list.style.visibility = "visible";
    }

    getFoodCount() {
        return this.list.children.length;
    }

    __createItem(simpleFood, foodNr) {
        var header = document.createElement("div"),
                body = document.createElement("div"),
                name = document.createTextNode(simpleFood.name),
                deleteButton = document.createElement("i"),
                collapsibleIcon = document.createElement("i"),
                details = document.createElement("p"),
                li = document.createElement("li")
        ,
                {kcal, carbohydrates, proteins, fats} = simpleFood,
        desc = `Kcal: ${kcal}, węglowodany: ${carbohydrates}g, białko: ${proteins}g, tłuszcze: ${fats}g`;

        details.appendChild(document.createTextNode(desc));

        deleteButton.className = "material-icons";
        deleteButton.innerHTML = "delete";
        deleteButton.setAttribute("food-id", simpleFood.id);
        deleteButton.addEventListener('click', e => this.__removeFood(simpleFood.id, foodNr, e));

        collapsibleIcon.className = "material-icons";
        collapsibleIcon.innerHTML = "play_arrow";

        header.className = "collapsible-header";
        header.appendChild(deleteButton);
        header.appendChild(name);
        header.appendChild(collapsibleIcon);

        body.className = "collapsible-body";
        body.appendChild(details);

        //we're returning list item
        li.setAttribute("food-id", simpleFood.id);
        li.setAttribute("food-index", this._foodIndex);
        li.appendChild(header);
        li.appendChild(body);

        return li;
    }

    __removeFood(foodId, foodIndex, e) {
        var listItem = $('[food-index="' + foodIndex + '"]');
        e.stopPropagation();

        UserMealService.removeFood(foodId, (removedFood, status) => {
            if (!$.isEmptyObject(removedFood) || status !== 'success') {

                listItem.slideUp(200, () => {
                    listItem.remove();
                    this.macro.remove(removedFood);

                    if (this.getFoodCount() < 1) {
                        this.list.style.visibility = "hidden";
                    }

                    for (let callback of this._onDelete) {
                        callback(foodId);
                    }
                });
            }
        });
    }
}





class FoodCarousel {
    constructor(container, navigation) {
        this._onItemClick = [];
        this.navigation = new CarouselNavigation(navigation);
        this.carousel = new Swiper(container, {
            direction: 'horizontal',
            grabCursor: true,
            mousewheelControl: true,
            slidesPerView: 3,
            freeMode: true,
            loop: true,
            observer: true,
            spaceBetween: 20,
            nextButton: this.navigation.rightArrow,
            prevButton: this.navigation.leftArrow,
            onClick: (swiper, event) => {
                for (let callback of this._onItemClick) {
                    let div = swiper.clickedSlide;
                    callback(div, event);
                }
            }
        });
    }

    initCarousel(foodList) {
        let foodElements = [];

        if (!foodList) {
            console.log("No food to initiaite carousel");
            return;
        }

        for (let food of foodList) {
            foodElements.push(this.__mapFoodToDiv(food));
        }

        this.removeCarousel();
        this.carousel.appendSlide(foodElements);
        if (this.navigation) {
            this.navigation.show();
        }
    }

    addOnItemClickListener(callback) {
        if (typeof callback === 'function') {
            this._onItemClick.push(callback);
        }
    }

    removeCarousel() {
        this.carousel.removeAllSlides();
        if (this.navigation) {
            this.navigation.hide();
        }
    }

    __mapFoodToDiv(simpleFood) {
        var img = document.createElement("img"),
                div = document.createElement("div"),
                foodName = document.createTextNode(simpleFood.name),
                textNode = document.createElement("p");

        textNode.appendChild(foodName);

        img.className = "food-image";
        img.setAttribute("src", "data:image/png;base64," + simpleFood.simpleImage.file);

        div.className = "food waves-effect waves-light swiper-slide";
        div.setAttribute("food-id", simpleFood.id);
        div.appendChild(img);
        div.appendChild(textNode);

        return div;
    }
}

class CarouselNavigation {
    constructor(container) {
        this.container = $(container);
        this.leftArrow = this.container.children().first();
        this.rightArrow = this.container.children().last();
    }

    show() {
        this.container.css("visibility", "visible");
    }

    hide() {
        this.container.css("visibility", "hidden");
    }
}




class FoodContainer {
    constructor(container) {
        this.container = $(container);
        this._onItemClick = [];
    }

    fill(foodList, perRow) {
        if (!foodList) {
            console.log("No food to initiaite carousel");
            return;
        }

        let rows = [],
                counter = 0,
                row = document.createElement("div");

        row.className = "row";
        for (let food of foodList) {
            row.appendChild(this.__mapFoodToDiv(food));
            counter++;

            if (counter === perRow) {
                counter = 0;
                rows.push(row);
                row = document.createElement("div");
                row.className = "row";
            }
        }

        if (counter > 0) {
            rows.push(row);
        }

        this.remove();
        this.container.append(rows);
    }

    addOnItemClickListener(callback) {
        if (typeof callback === 'function') {
            this._onItemClick.push(callback);
        }
    }

    remove() {
        this.container.empty();
    }

    __mapFoodToDiv(simpleFood) {
        var img = document.createElement("img"),
                div = document.createElement("div"),
                foodName = document.createTextNode(simpleFood.name),
                textNode = document.createElement("p");

        textNode.appendChild(foodName);

        img.className = "food-image";
        img.setAttribute("src", "data:image/png;base64," + simpleFood.simpleImage.file);

        div.className = "food waves-effect waves-light";
        div.setAttribute("food-id", simpleFood.id);
        div.appendChild(img);
        div.appendChild(textNode);
        div.addEventListener('click', e => {
            for (let callback of this._onItemClick) {
                callback(div, e);
            }
        });

        return div;
    }

}


/*****************************************************************************
 
 ******************************************************************************/

var MY_FRAMEWORK = MY_FRAMEWORK || {};
MY_FRAMEWORK.application = {
    restaurantNavigation: new RestaurantNavigation("#restaurant-list"),
    categoriesSelect: new CategoriesSelect("#category-select"),
    userMealList: new UserMealList("#user-meal-list"),
    foodContainer: new FoodContainer("#food-list-bigscreen"), //only on big screen
    resultBar: new ResultBar("#result-bar", "#footer"),
    foodCarousel: new FoodCarousel("#food-list", "#carousel-navigation"), //only medium and small screen
    preloader: new Preloader("#preloader"),

    includeCsrf() {
        var token = $("meta[name='_csrf']").attr("content"),
                header = $("meta[name='_csrf_header']").attr("content");

        $(document).ajaxSend((e, xhr, options) => {
            xhr.setRequestHeader(header, token);
        });
    },

    setRestaurantNavigation() {
        var isFatAlbertVisible = true;
        this.restaurantNavigation.addOnClickListener((radioButton, e) => {
            this.preloader.show();

            if (isFatAlbertVisible) {
                isFatAlbertVisible = false;
                $("#fat-albert").addClass("hidden");
            }
            let restaurant = radioButton.getAttribute('value');
            this.foodCarousel.removeCarousel();
            this.foodContainer.remove();
            RestaurantsService.getMenu(restaurant, menu => {
                $("#fat-albert").css("position", "absolute");

                this.categoriesSelect.fill(menu, true);
                this.categoriesSelect.selectOption(0);
                this.preloader.hide();
            });
        });
    },

    setCategoriesSelect() {
        this.categoriesSelect.addOnClickListener(categoryId => {
            let currentRestaurant = this.restaurantNavigation.selectedRestaurantName();
            RestaurantsService.getFoods(currentRestaurant, categoryId, foodList => {
                this.categoriesSelect.disable();
                this.foodCarousel.initCarousel(foodList);
                this.foodContainer.fill(foodList, 4);
                this.categoriesSelect.enable();
            });
        });
    },

    setUserMeal() {
        this.userMealList.addOnDeleteListener(() => {
            this.resultBar.setUserMacro(this.userMealList.macro);
        });
    },

    setFoodCarousel() {
        this.foodCarousel.addOnItemClickListener(element => {
            let foodId = element.getAttribute("food-id");
            this.__addFood(foodId);
        });
    },

    setFoodContainer() {
        this.foodContainer.addOnItemClickListener(element => {
            let foodId = element.getAttribute("food-id");
            this.__addFood(foodId);
        });
    },

    __addFood(foodId) {
        UserMealService.addToMeal(foodId, (simpleFood, status) => {
            if (simpleFood && status) { //not null nor empty
                this.userMealList.addToUserMeal(simpleFood);
                this.resultBar.showResultBar();
                this.resultBar.setUserMacro(this.userMealList.macro);
                if (MediaQueries.medium().matches) {
                    Materialize.toast(simpleFood.name, 3000);
                }
            }
        });
    },

    setBottomModalWindow() {
        var macro = this.userMealList.macro;
        ActivitiesService.loadAndCacheActivities();
        $('#modal').modal({
            ready() {
                document.querySelector(".modal-content > p").innerHTML = `aby spalić <em>` + macro.kcal + "</em> kilokalorii możesz np.:";
                let emptyList = $("#activities-list").empty();
                let items = ActivityCalculator.getRandomItems(5, macro.kcal);
                emptyList.append(items);
            }
        });
    },

    init() {
        this.includeCsrf();
        this.setRestaurantNavigation();
        this.setCategoriesSelect();
        this.setUserMeal();
        this.setFoodCarousel();
        this.setFoodContainer();
        $('.collapsible').collapsible();

        //load user meal if exists
        this.userMealList.loadUserMeal(foods => {
            if (foods.length > 0) {
                this.resultBar.showResultBar();
                this.resultBar.setUserMacro(this.userMealList.macro);
            }

            //Materialize staggered list effect
            let listItems = this.userMealList.list.querySelectorAll('li');
            for (let li of listItems) {
                li.style.opacity = 0;
            }
            Materialize.showStaggeredList("#" + this.userMealList.list.getAttribute("id"));
        });

        this.setBottomModalWindow();
    }
};

//--- START
$(() => MY_FRAMEWORK.application.init());