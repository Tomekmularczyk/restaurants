/* global Materialize */

$(() => {
    $('input.autocomplete').autocomplete({
        data: {
            guest: null
        }
    });
    
    $('#modal').modal();
    
    let status = $("#redirect-message > .status").text();
    
    if (status === "error") {
        document.getElementById("message").innerHTML = "Nie udało się zalogować";
        document.getElementById("suggestion").innerHTML = "Spróbuj jeszcze raz...";
        $('#modal').modal('open');
    } else if (status === "logout") {
        document.getElementById("message").innerHTML = "Zostałeś wylogowany";
        document.getElementById("suggestion").innerHTML = '<a href="/">wróć do głównej strony…</a>';
        $('#modal').modal('open');
    } else {
        Materialize.toast('Zwiedzający?<br/> login: guest, hasło: guest', 5000);
    }
});