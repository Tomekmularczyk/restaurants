/* global Materialize */

class ModalWindow {
    constructor(container, confirmButtonId) {
        this.container = $(container);
        this.container.modal();
        if (confirmButtonId) {
            this.confirmButton = $(confirmButtonId);
        }
    }

    setConfirmUrl(url) {
        this.confirmButton.attr("href", url);
    }

    addConfirmListener(callback) {
        if (typeof callback === 'function') {
            this.confirmButton.click(callback);
        }
    }

    show() {
        this.container.modal("open");
    }

    setMessage(header, message) {
        var content = this.container.find(".modal-content").children();
        content.first().text(header);
        content.last().text(message);
    }
}


var MY_FRAMEWORK = MY_FRAMEWORK || {};

MY_FRAMEWORK.global = {
    materialize() {
        $('#main-tabs').tabs();
        $('.secondary-tabs').tabs();
        $('select').material_select();

        $('.button-collapse').sideNav({
            menuWidth: 250, // Default is 240
            edge: 'left',
            closeOnClick: false,
            draggable: true
        });

        $('.side-nav .action-link').on('click', e => { //fix
            let windowsize = $(window).width();
            if (windowsize < 992) {
                $('.button-collapse').sideNav('hide');
            }
        });
    },

    includeCsrf() {
        var token = $("meta[name='_csrf']").attr("content"),
                header = $("meta[name='_csrf_header']").attr("content");

        $(document).ajaxSend((e, xhr, options) => {
            xhr.setRequestHeader(header, token);
        });
    },

    init() {
        this.includeCsrf();
        this.materialize();
    }
};

MY_FRAMEWORK.sidenav = {

    setLogout() {
        $("#logout").click(e => {
            document.forms['logout-form'].submit();
        });
    },

    setNavigationDropdown() {
        $("#show-foods").click(e => {
            setActive(e.target);
            $('#main-tabs').tabs('select_tab', 'tab-foods');
        });

        $("#show-categories").click(e => {
            setActive(e.target);
            $('#main-tabs').tabs('select_tab', 'tab-categories');
        });

        $("#show-activities").click(e => {
            setActive(e.target);
            $('#main-tabs').tabs('select_tab', 'tab-activities');
        });

        function setActive(element) {
            var parent = $(element).parent();
            parent.siblings().removeClass("active");
            parent.addClass("active");
        }
    },

    setDatabaseDropdown() {
        var confirmWindow = new ModalWindow("#confirmation-modal", "#modal-ok"),
                infoWindow = new ModalWindow("#info-modal");

        confirmWindow.addConfirmListener(e => {
            e.preventDefault();
            var link = $(e.target).attr("href");

            $.get(link, (data, status) => {
                if (data.status === 'GUEST') {
                    Materialize.toast("Gościom nie pozwalamy na dostęp do bazy ;)", 4000);
                } else if (data.status === 'SUCCESS') {
                    infoWindow.setMessage("Sukces!", "Operacja zakończyła się powodzeniem.");
                    infoWindow.show();
                }
            }).fail((xhr, textStatus, error) => {
                infoWindow.setMessage("Błąd.", textStatus + error);
                infoWindow.show();
            });
        });

        $("#load-foods").click(e => {
            confirmWindow.setConfirmUrl("/admin/load-food-xlsx");
            confirmWindow.setMessage("Jesteś pewny?", "Po tej operacji zawartość tabeli z jedzeniem zostanie usunięta i przeładowana.");
            confirmWindow.show();
        });

        $("#load-categories").click(e => {
            confirmWindow.setConfirmUrl("/admin/load-categories-xlsx");
            confirmWindow.setMessage("Jesteś pewny?", "Po tej operacji zawartość tabeli z kategoriami zostanie usunięta i przeładowana.");
            confirmWindow.show();
        });

        $("#load-activities").click(e => {
            confirmWindow.setConfirmUrl("/admin/load-activities-xlsx");
            confirmWindow.setMessage("Jesteś pewny?", "Po tej operacji zawartość tabeli z aktywnościami zostanie usnięta i przeładowana.");
            confirmWindow.show();
        });

    },

    init() {
        this.setLogout();
        this.setNavigationDropdown();
        this.setDatabaseDropdown();
    }
};

MY_FRAMEWORK.forms = {
    categoriesCache: {},
    foodsCache: {},

    setInsertTabs() {
        var select = $('#restaurant-select-foods');
        select.change(e => {
            let restaurant = $(e.target).val(),
                    cachedCategories = this.categoriesCache[restaurant],
                    url = "/food-categories/" + restaurant;

            if (cachedCategories) {
                poluteCategoriesSelect(cachedCategories);
            } else {
                $.getJSON(url, categories => {
                    this.categoriesCache[restaurant] = categories;
                    poluteCategoriesSelect(categories);
                });
            }
        });

        select.val("MC_DONALD").change();

        function poluteCategoriesSelect(categories) {
            var categoriesSelect = $("#food-categories"),
                    options = [];
            categoriesSelect.material_select('destroy');

            categoriesSelect.children().not(":first").remove();
            for (let category of categories) {
                let option = document.createElement("option");
                option.innerHTML = category;
                option.value = category.id;
                option.text = category.name;
                options.push(option);
            }

            categoriesSelect.append(options);
            categoriesSelect.material_select();
        }
    },

    setEditTabs() {
        $("#food-edit > .restaurant-select").change(e => {
            let table = $("#foods-table tbody"),
                    restaurant = $(e.target).val(),
                    cachedMenu = this.foodsCache[restaurant],
                    url = `/list-foods?restaurant=${restaurant}`;


            table.empty();
            if (cachedMenu) {
                let tableRows = mapEachCategory(cachedMenu);
                table.append(tableRows);
            } else {
                $.get(url, menu => {
                    this.foodsCache[restaurant] = menu;
                    let tableRows = mapEachCategory(menu);
                    table.append(tableRows);
                });
            }

            function mapEachCategory(menu) {
                let trows = [];
                Object.keys(menu).forEach((key, index) => {
                    let foods = menu[key];
                    for (let food of foods) {
                        trows.push(mapFoodToTR(food));
                    }
                });
                return trows;
            }

            function mapFoodToTR(food) {
                let tr = document.createElement("tr"),
                        tdata = [];

                tdata.push("<td>" + food.id + "</td>");
                tdata.push("<td>" + new Date(food.created).toDateString() + "</td>");
                tdata.push("<td>" + food.restaurant + "</td>");
                tdata.push("<td>" + food.category.id + "</td>");
                tdata.push("<td>" + food.category.name + "</td>");
                tdata.push("<td>" + food.carbohydrates + "</td>");
                tdata.push("<td>" + food.kcal + "</td>");
                tdata.push("<td>" + food.proteins + "</td>");
                tdata.push("<td>" + food.fats + "</td>");

                tr.innerHTML = tdata.join("");
                return tr;
            }
        });

    },

    setFormSubmissionThroughAjax() {
        let infoWindow = new ModalWindow("#info-modal");

        $("form").submit(e => {
            let url = $(e.target).attr("action");

            $.ajax({
                type: "POST",
                url,
                data: $(e.target).serialize() // serializes the form's elements.
            }).done((data, text) => {
                Materialize.toast("Zakończono powodzeniem.", 3000);
            }).fail((xhr, textStatus, error) => {
                infoWindow.setMessage("Błąd.", textStatus + error);
                infoWindow.show();
            });

            e.preventDefault();
        });
    },

    init() {
        this.setInsertTabs();
        this.setEditTabs();
        this.setFormSubmissionThroughAjax();
    }
};

$(() => {
    MY_FRAMEWORK.global.init();
    MY_FRAMEWORK.sidenav.init();
    MY_FRAMEWORK.forms.init();
});