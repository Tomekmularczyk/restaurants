package bedegruba.domain;

import bedegruba.entities.simple.SimpleFood;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * Stores combination of foods selected by user
 */
@Component
@Scope(scopeName = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserMeal {

    private List<SimpleFood> meal;

    @PostConstruct
    public void init() {
        meal = new LinkedList<>();
    }

    public List<SimpleFood> getMeal() {
        return meal;
    }

    public void addToMeal(SimpleFood simpleFood) {
        Objects.requireNonNull(simpleFood, "food is required");
        meal.add(simpleFood);
    }

    public Optional<SimpleFood> removeFromMeal(int foodId) {
        return meal.stream()
                .filter(f -> f.getId().equals(foodId))
                .findFirst()
                .filter(meal::remove);
    }

    public Macro getMacro() {
        Macro.Builder builder = Macro.builder();

        for (SimpleFood simpleFood : meal) {
            builder.addKcal(simpleFood.getKcal());
            builder.addCarbohydrates(simpleFood.getCarbohydrates());
            builder.addProteins(simpleFood.getProteins());
            builder.addFats(simpleFood.getFats());
        }

        return builder.build();
    }

    public static class Macro {

        public final int kcal;
        public final int carbohydrates;
        public final int proteins;
        public final int fats;

        private Macro(Builder builder) {
            this.kcal = Math.round(builder.kcal);
            this.carbohydrates = Math.round((float) builder.carbohydrates);
            this.proteins = Math.round((float) builder.proteins);
            this.fats = Math.round((float) builder.fats);
        }

        public static Builder builder() {
            return new Builder();
        }

        public static class Builder {

            private int kcal;
            private double carbohydrates;
            private double proteins;
            private double fats;

            public Builder() {
                kcal = 0;
                carbohydrates = 0;
                proteins = 0;
                fats = 0;
            }

            public void addCarbohydrates(double carbohydrates) {
                this.carbohydrates += carbohydrates;
            }

            public void addProteins(double proteins) {
                this.proteins += proteins;
            }

            public void addFats(double fats) {
                this.fats += fats;
            }

            public void addKcal(int kcal) {
                this.kcal += kcal;
            }

            public Macro build() {
                return new Macro(this);
            }
        }

        public int getKcal() {
            return kcal;
        }

        public double getCarbohydrates() {
            return carbohydrates;
        }

        public double getProteins() {
            return proteins;
        }

        public double getFats() {
            return fats;
        }
    }
}
