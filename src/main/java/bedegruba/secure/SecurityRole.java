package bedegruba.secure;

public enum SecurityRole {
    ADMIN,
    GUEST
}
