package bedegruba.rest;

import bedegruba.entities.simple.SimpleActivity;
import bedegruba.entities.simple.SimpleCategory;
import bedegruba.entities.simple.SimpleFood;
import bedegruba.entities.simple.SimpleImage;
import bedegruba.secure.SecurityRole;
import bedegruba.services.ActivityService;
import bedegruba.services.CategoryService;
import bedegruba.services.FoodService;
import bedegruba.services.XlsxActivityService;
import bedegruba.services.XlsxCategoryService;
import bedegruba.services.XlsxFoodService;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static bedegruba.rest.SimpleStatusObject.Status;

@RestController
@RequestMapping("/admin")
public class AdminRest {

    private static final Logger LOG = Logger.getLogger(AdminRest.class);

    @Autowired
    private FoodService foodService;
    @Autowired
    private CategoryService categoriesService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private XlsxFoodService xlsxFoodService;
    @Autowired
    private XlsxCategoryService xlsxCategoryService;
    @Autowired
    private XlsxActivityService xlsxActivityService;

    @FunctionalInterface
    private interface Action {
        void run();
    }

    @RequestMapping("/save-food")
    public SimpleStatusObject saveFood(HttpServletRequest request, final SimpleFood simpleFood) {
        return checkPrivilegesAndRun(request, () -> {
            SimpleImage simpleImage = SimpleImage.of(simpleFood.getMultipartImage());
            simpleFood.setSimpleImage(simpleImage);
            foodService.saveFood(simpleFood);
        });
    }

    @RequestMapping("/save-category")
    public SimpleStatusObject saveCategory(HttpServletRequest request, final SimpleCategory simpleCategory) {
        return checkPrivilegesAndRun(request, () -> {
            categoriesService.save(simpleCategory);
        });
    }

    @RequestMapping("/save-activity")
    public SimpleStatusObject saveActivity(HttpServletRequest request, final SimpleActivity simpleActivity) {
        return checkPrivilegesAndRun(request, () -> {
            activityService.save(simpleActivity);
        });
    }

    @RequestMapping("/load-food-xlsx")
    public SimpleStatusObject loadFoodFromXlsx(HttpServletRequest request) {
        return checkPrivilegesAndRun(request, () -> {
            foodService.deleteAllFoods();
            xlsxFoodService.loadFoodsToDtbs();
        });
    }

    @RequestMapping("/load-categories-xlsx")
    public SimpleStatusObject loadCategoriesXlsx(HttpServletRequest request) {
        return checkPrivilegesAndRun(request, () -> {
            categoriesService.deleteAllCategories();
            xlsxCategoryService.loadCategoriesToDtbs();
        });
    }

    @RequestMapping("/load-activities-xlsx")
    public SimpleStatusObject loadActivitesXlsx(HttpServletRequest request) {
        return checkPrivilegesAndRun(request, () -> {
            activityService.deleteAllActivities();
            xlsxActivityService.loadActivitiesToDtbs();
        });
    }

    private SimpleStatusObject checkPrivilegesAndRun(HttpServletRequest request, Action action) {
        SimpleStatusObject statusObject = new SimpleStatusObject();
        if (request.isUserInRole(SecurityRole.ADMIN.toString())) {
            action.run();
            statusObject.setStatus(Status.SUCCESS);
        } else {
            LOG.info("Action not commited - GUEST user.");
            statusObject.setStatus(Status.GUEST);
        }

        return statusObject;
    }
}
