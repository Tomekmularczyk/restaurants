package bedegruba.rest;

import bedegruba.entities.simple.SimpleActivity;
import bedegruba.services.ActivityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActivityRest {
    @Autowired
    private ActivityService activityService;
    
    @RequestMapping("/get-activities")
    public List<SimpleActivity> getActivities() {
        return activityService.getAllActivities();
    }
}
