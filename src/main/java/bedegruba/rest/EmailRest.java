package bedegruba.rest;

import bedegruba.entities.simple.SimpleEmail;
import bedegruba.rest.SimpleStatusObject.Status;
import bedegruba.services.EmailService;
import java.io.UnsupportedEncodingException;
import javax.mail.MessagingException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailRest {
    
    private static final Logger LOG = Logger.getLogger(EmailRest.class);
    
    @Autowired
    private EmailService emailService;
    
    @RequestMapping("/contact-email")
    public SimpleStatusObject sendMail(final SimpleEmail simpleEmail) {
        SimpleStatusObject status = new SimpleStatusObject();
        try {
            return emailService.sendEmail(simpleEmail);
        } catch (UnsupportedEncodingException | MessagingException ex) {
            status.setStatus(Status.ERROR);
            status.setMessage(ex.getMessage());
            LOG.error("Błąd podczas wysyłania wiadomości.", ex);
        }
        
        return status;
    }
}
