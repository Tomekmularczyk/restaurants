package bedegruba.rest;

import bedegruba.domain.UserMeal;
import bedegruba.entities.simple.SimpleFood;
import bedegruba.services.FoodService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserMealRest {

    @Autowired
    private UserMeal userMeal;
    @Autowired
    private FoodService foodService;

    @RequestMapping(path = "/add-to-meal", method = RequestMethod.POST)
    public Optional<SimpleFood> addFood(@RequestParam("foodId") int foodId) {
        return foodService.findFood(foodId)
                .filter(simpleFood -> {
                    userMeal.addToMeal(simpleFood);
                    return true;
                });
    }

    @RequestMapping(path = "/get-meal", method = RequestMethod.POST) // csrf protection
    public List<SimpleFood> getMeal() {
        return userMeal.getMeal();
    }

    @RequestMapping(path = "/get-macro", method = RequestMethod.POST)
    public UserMeal.Macro getMacro() {
        return userMeal.getMacro();
    }

    @RequestMapping(path = "/remove-from-meal", method = RequestMethod.POST)
    public Optional<SimpleFood> removeFromMeal(@RequestParam("foodId") int foodId) {
        return userMeal.removeFromMeal(foodId);
    }
}
