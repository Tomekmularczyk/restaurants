package bedegruba.rest;

import bedegruba.domain.Restaurant;
import bedegruba.entities.simple.SimpleCategory;
import bedegruba.entities.simple.SimpleFood;
import bedegruba.services.CategoryService;
import bedegruba.services.FoodService;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FoodRest {

    @Autowired
    private FoodService foodService;
    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/list-foods")
    public Map<SimpleCategory, List<SimpleFood>> foodList(@RequestParam(value = "restaurant") String restaurant) {
        return foodService.findByRestaurantGroupByCategory(Restaurant.valueOf(restaurant.toUpperCase()));
    }

    @RequestMapping("/restaurants")
    public List<Restaurant> restaurants() {
        return Arrays.asList(Restaurant.values());
    }
    
    @RequestMapping("/food-categories/{restaurant}")
    public List<SimpleCategory> categories(@ModelAttribute Restaurant restaurant) {
        return categoryService.findBy(restaurant);
    }
}
