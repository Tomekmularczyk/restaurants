package bedegruba.rest;

public class SimpleStatusObject {

    public SimpleStatusObject() {
    }
    
    public static enum Status {
        ERROR, SUCCESS, GUEST
    }
    
    private Status status;
    private String message;

    public SimpleStatusObject(Status status, String message) {
        this.status = status;
        this.message = message;
    }
    
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
