package bedegruba.exceptionhandlers;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Catches exception that were not handled by programmer
 */
@Controller
public class ApplicationErrorController implements ErrorController{

    private static final String ERROR_PATH = "/error";
    
    @Autowired
    private ErrorAttributes errorAttributes;
    
    @RequestMapping(ERROR_PATH)
    public String errorPage(HttpServletRequest request, HttpServletResponse response, Model model){
        ServletRequestAttributes reqAttributes = new ServletRequestAttributes(request);
        Map<String, Object> error = errorAttributes.getErrorAttributes(reqAttributes, true);
        model.addAttribute("error", error);
        response.setStatus(500);
        return "error";
    }
    
    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }

}
