package bedegruba.exceptionhandlers;

import bedegruba.controllers.Admin;
import bedegruba.rest.SimpleStatusObject;
import bedegruba.rest.SimpleStatusObject.Status;
import bedegruba.services.StorageException;
import bedegruba.services.XlsxException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ExceptionHandlerService extends ResponseEntityExceptionHandler {

    private static final Logger LOG = Logger.getLogger(Admin.class);
    
    @Value("${spring.http.multipart.max-file-size}")
    private String imgMaxSize;

    @ExceptionHandler(SQLException.class)
    public SimpleStatusObject exception(HttpServletRequest req, HttpServletResponse response, Exception ex) {
        LOG.error("Request: " + req.getRequestURL() + " raised exception\n", ex);
        String message = ex.getClass().getSimpleName() + "-" + ex.getMessage();
        response.setStatus(500);
        return new SimpleStatusObject(Status.ERROR, message);
    }

    @ExceptionHandler(StorageException.class)
    public SimpleStatusObject storageException(HttpServletRequest req, HttpServletResponse response, Exception ex) {
        LOG.error("Request: " + req.getRequestURL() + " raised exception\n", ex);
        String message = ex.getCause().getClass().getSimpleName() + "-" + ex.getMessage();
        response.setStatus(500);
        return new SimpleStatusObject(Status.ERROR, message);
    }

    @ExceptionHandler(MultipartException.class)
    public SimpleStatusObject handleFileException(HttpServletRequest req, HttpServletResponse response, Throwable ex) {
        LOG.error("Request: " + req.getRequestURL() + " raised exception\n", ex);
        response.setStatus(500);
        return new SimpleStatusObject(Status.ERROR, "file size cannot be bigger than " + imgMaxSize);
    }

    @ExceptionHandler(XlsxException.class)
    public SimpleStatusObject handleXlsxException(HttpServletRequest req, HttpServletResponse response, Throwable ex){
        LOG.error("Request: " + req.getRequestURL() + " raised exception\n", ex);
        String message = ex.getCause().getClass().getSimpleName() + "-" + ex.getMessage();
        response.setStatus(500);
        return new SimpleStatusObject(Status.ERROR, message);
    }
}