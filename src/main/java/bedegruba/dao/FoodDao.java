package bedegruba.dao;

import bedegruba.entities.Food;
import bedegruba.domain.Restaurant;
import bedegruba.entities.Category;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodDao extends JpaRepository<Food, Integer>{

    List<Food> findByRestaurant(Restaurant restaurant);
    
    List<Food> findByCategory(Category category);
}
