package bedegruba.dao;

import bedegruba.entities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityDao extends JpaRepository<Activity, Integer>{
    
}
