package bedegruba.dao;

import bedegruba.entities.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageDao extends JpaRepository<Image, Integer>{
    
    Image findByFoodId(int foodId);
    
}
