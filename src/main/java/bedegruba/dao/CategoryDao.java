package bedegruba.dao;

import bedegruba.domain.Restaurant;
import bedegruba.entities.Category;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryDao extends JpaRepository<Category, Integer> {

    List<Category> findByRestaurantOrderById(Restaurant restaurant);
}
