package bedegruba.services;

import bedegruba.dao.ActivityDao;
import bedegruba.entities.Activity;
import bedegruba.entities.simple.SimpleActivity;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ActivityService {
    
    @Autowired
    private ActivityDao activityDao;
    
    public List<SimpleActivity> getAllActivities() {
        return SimpleActivity.of(activityDao.findAll());
    }
    
    public void deleteAllActivities(){
        activityDao.deleteAll();
    }
    
    public SimpleActivity save(SimpleActivity simpleActivity) {
        Activity activity = new Activity();
        activity.setName(simpleActivity.getName());
        activity.setDuration(simpleActivity.getDuration());
        return SimpleActivity.of(activityDao.save(activity));
    }
    
    public List<SimpleActivity> save(List<SimpleActivity> simpleActivities) {        
        return simpleActivities.stream()
                .map(this::save)
                .collect(Collectors.toList());
    }
}
