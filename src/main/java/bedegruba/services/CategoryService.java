package bedegruba.services;

import bedegruba.dao.CategoryDao;
import bedegruba.domain.Restaurant;
import bedegruba.entities.Category;
import bedegruba.entities.simple.SimpleCategory;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CategoryService {
    
    @Autowired
    private CategoryDao categoryDao;
    
    public void deleteAllCategories() {
        categoryDao.deleteAll();
    }
 
    public SimpleCategory save(SimpleCategory simpleCategory){
        Category category = new Category();
        category.setName(simpleCategory.getName());
        category.setRestaurant(simpleCategory.getRestaurant());
        return SimpleCategory.of(categoryDao.save(category));
    }
    
    public SimpleCategory findBy(int id){
        return SimpleCategory.of(categoryDao.findOne(id));
    }
    
    public List<SimpleCategory> findBy(Restaurant restaurant){
        return SimpleCategory.of(categoryDao.findByRestaurantOrderById(restaurant));
    }
    
    public List<SimpleCategory> getALlCategories(){        
        return Arrays.stream(Restaurant.values())
                .map(this::findBy)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }
}
