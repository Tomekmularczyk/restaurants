package bedegruba.services;

import bedegruba.dao.CategoryDao;
import bedegruba.dao.FoodDao;
import bedegruba.dao.ImageDao;
import bedegruba.entities.Food;
import bedegruba.entities.Image;
import bedegruba.domain.Restaurant;
import bedegruba.entities.Category;
import bedegruba.entities.simple.SimpleCategory;
import bedegruba.entities.simple.SimpleFood;
import bedegruba.entities.simple.SimpleImage;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class FoodService {

    @Autowired
    private FoodDao foodDao;
    @Autowired
    private ImageDao imageDao;
    @Autowired
    private CategoryDao categoryDao;

    public List<SimpleFood> saveFoods(List<SimpleFood> simpleFoods) {
        return simpleFoods.stream()
                .map(this::saveFood)
                .collect(Collectors.toList());
    }

    public SimpleFood saveFood(SimpleFood simpleFood) {
        return SimpleFood.of(insert(simpleFood));
    }

    private Food insert(SimpleFood simpleFood) {
        Food entity = new Food();
        entity.setRestaurant(simpleFood.getRestaurant());
        entity.setName(simpleFood.getName());
        Category category = categoryDao.findOne(simpleFood.getCategory().getId());
        entity.setCategory(category);
        entity.setKcal(simpleFood.getKcal());
        entity.setCarbohydrates(simpleFood.getCarbohydrates());
        entity.setFats(simpleFood.getFats());
        entity.setProteins(simpleFood.getProteins());

        Image image = insert(simpleFood.getSimpleImage());
        image.setFood(entity);
        entity.setImage(image);

        return foodDao.save(entity);
    }

    private Image insert(SimpleImage simpleImage) {
        Image image = new Image(simpleImage.getName(), simpleImage.getFile());
        return imageDao.save(image);
    }

    public List<SimpleFood> findByRestaurant(Restaurant restaurant) {
        return foodDao.findByRestaurant(restaurant)
                .stream()
                .map(SimpleFood::of)
                .collect(Collectors.toList());
    }

    public Optional<SimpleFood> findFood(int foodId) {
        return Optional.ofNullable(foodDao.findOne(foodId))
                .map(SimpleFood::of);
    }

    public Map<SimpleCategory, List<SimpleFood>> findByRestaurantGroupByCategory(Restaurant restaurant) {
        return categoryDao.findByRestaurantOrderById(restaurant)
                .stream()
                .collect(Collectors.toMap(SimpleCategory::of , c -> SimpleFood.of(foodDao.findByCategory(c))));
    }

    public void deleteAllFoods() {
        foodDao.deleteAll();
    }

}
