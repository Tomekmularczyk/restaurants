package bedegruba.services;

import bedegruba.entities.simple.SimpleActivity;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class XlsxActivityService {

    private static final Logger LOG = Logger.getLogger(XlsxActivityService.class);

    @Value("${xlsx.root.path}")
    private String xlsxPath;

    @Autowired
    private ActivityService activityService;

    public void loadActivitiesToDtbs() {
        String path = xlsxPath + "activities.xlsx";
        try (InputStream xlsx = getClass().getClassLoader().getResourceAsStream(path)) {
            if (xlsx != null) {
                Workbook workbook = new XSSFWorkbook(xlsx);
                Sheet sheet = workbook.getSheetAt(0);
                Iterator<Row> rowIterator = sheet.iterator();

                List<SimpleActivity> simpleActivites = new LinkedList<>();
                rowIterator.forEachRemaining(row -> {
                    simpleActivites.add(mapToSimpleActivity(row));
                });

                activityService.save(simpleActivites);
            }
        } catch (IOException ex) {
            LOG.error("Error while loading data from xlsx file. Data could be incorrectly formatted.", ex);
        }
    }

    private SimpleActivity mapToSimpleActivity(Row row) {
        Iterator<Cell> cellIterator = row.cellIterator();
        String name = cellIterator.next().getStringCellValue();
        int duration = (int) cellIterator.next().getNumericCellValue();

        return new SimpleActivity(-1, name, duration);
    }

}
