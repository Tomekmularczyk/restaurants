package bedegruba.services;

import bedegruba.entities.simple.SimpleEmail;
import bedegruba.rest.SimpleStatusObject;
import bedegruba.rest.SimpleStatusObject.Status;
import java.io.UnsupportedEncodingException;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Value("${spring.mail.username}")
    private String recipient;

    @Autowired
    private JavaMailSender javaMailSender;

    public SimpleStatusObject sendEmail(SimpleEmail simpleEmail) throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(recipient);
        helper.setFrom("MOJA_STRONA");
        helper.setReplyTo(simpleEmail.getSenderEmail(), simpleEmail.getSenderName());
        helper.setSubject(simpleEmail.getSubject());
        helper.setText(simpleEmail.getMessage());

        javaMailSender.send(message);
        return new SimpleStatusObject(Status.SUCCESS, "message sent");
    }
}
