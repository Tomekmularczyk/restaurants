package bedegruba.services;

public class XlsxException extends RuntimeException{

    public XlsxException(String message) {
        super(message);
    }

    public XlsxException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
