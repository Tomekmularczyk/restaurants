package bedegruba.services;

import bedegruba.domain.Restaurant;
import bedegruba.entities.simple.SimpleFood;
import bedegruba.entities.simple.SimpleImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class XlsxFoodService {

    private static final Logger LOG = Logger.getLogger(XlsxFoodService.class);

    @Value("${xlsx.root.path}")
    private String xlsxPath;

    @Autowired
    private FoodService foodService;
    @Autowired
    private CategoryService categoryService;

    public void loadFoodsToDtbs() {
        for (Restaurant restaurant : Restaurant.values()) {
            String path = xlsxPath + restaurant.name() + ".xlsx";
            try (InputStream xlsx = getClass().getClassLoader().getResourceAsStream(path)) {
                if (xlsx != null) {
                    Workbook workbook = new XSSFWorkbook(xlsx);
                    List<SimpleFood> foods = loadFromWorkbook(restaurant, workbook);
                    foodService.saveFoods(foods);
                }
            } catch (IOException ex) {
                LOG.error("Error while loading data from xlsx file. Data could be incorrectly formatted.", ex);
            }
        }
    }

    private List<SimpleFood> loadFromWorkbook(Restaurant restaurant, Workbook workbook) {
        List<SimpleFood> foodList = new LinkedList<>();
        Sheet sheet = workbook.getSheetAt(0);

        Iterator<Row> rowIterator = sheet.iterator();
        Iterator<? extends PictureData> pictureIterator = workbook.getAllPictures().iterator();

        rowIterator.next(); //because first 2 rows is metadata
        rowIterator.next();
        while (rowIterator.hasNext() && pictureIterator.hasNext()) {
            Row row = rowIterator.next();
            PictureData image = pictureIterator.next();
            SimpleFood simpleFood = mapToSimpleFood(restaurant, row, image);
                foodList.add(simpleFood);
            }

        return foodList;
    }

    private SimpleFood mapToSimpleFood(Restaurant restaurant, Row row, PictureData image) {
        Iterator<Cell> cellIterator = row.cellIterator();

        String name = cellIterator.next().getStringCellValue();
        int categoryId = (int) cellIterator.next().getNumericCellValue();
        double kcal = cellIterator.next().getNumericCellValue();
        double carbohydrates = cellIterator.next().getNumericCellValue();
        double proteins = cellIterator.next().getNumericCellValue();
        double fats = cellIterator.next().getNumericCellValue();

        SimpleFood simpleFood = new SimpleFood();
        simpleFood.setRestaurant(restaurant);
        simpleFood.setName(name);
        simpleFood.setCategory(categoryService.findBy(categoryId));
        simpleFood.setKcal((int) kcal);
        simpleFood.setCarbohydrates(carbohydrates);
        simpleFood.setProteins(proteins);
        simpleFood.setFats(fats);

        String imageName = new Date().getTime() + "_" + name;
        simpleFood.setSimpleImage(new SimpleImage(imageName, image.getData()));

        return simpleFood;
    }

}
