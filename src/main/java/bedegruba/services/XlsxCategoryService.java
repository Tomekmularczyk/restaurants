package bedegruba.services;

import bedegruba.domain.Restaurant;
import bedegruba.entities.simple.SimpleCategory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class XlsxCategoryService {

    private static final Logger LOG = Logger.getLogger(XlsxCategoryService.class);

    @Value("${xlsx.root.path}")
    private String xlsxPath;

    @Autowired
    private CategoryService categoryService;

    public void loadCategoriesToDtbs() {
        String path = xlsxPath + "categories.xlsx";
        try (InputStream xlsx = getClass().getClassLoader().getResourceAsStream(path)) {
            if (xlsx != null) {
                Workbook workbook = new XSSFWorkbook(xlsx);
                for (Restaurant restaurant : Restaurant.values()) {
                    Sheet sheet = workbook.getSheet(restaurant.name());
                    if(sheet != null){
                        Iterator<Row> iterator = sheet.iterator();
                        iterator.forEachRemaining(row -> {
                            SimpleCategory simpleCategory = mapToSimpleCategory(restaurant, row);
                            categoryService.save(simpleCategory);
                        });
                    }
                }
            }
        } catch (IOException ex) {
            LOG.error("Error while loading data from xlsx file. Data could be incorrectly formatted.", ex);
        }
    }

    private SimpleCategory mapToSimpleCategory(Restaurant restaurant, Row row) {
        Iterator<Cell> cellIterator = row.cellIterator();
        String name = cellIterator.next().getStringCellValue();
        int id = (int) cellIterator.next().getNumericCellValue();
        
        return new SimpleCategory(id, name, restaurant);
    }
}
