package bedegruba.controllers;

import bedegruba.domain.Restaurant;
import bedegruba.entities.simple.SimpleActivity;
import bedegruba.entities.simple.SimpleCategory;
import bedegruba.entities.simple.SimpleFood;
import bedegruba.services.ActivityService;
import bedegruba.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class Admin {

    @Autowired
    private ActivityService activityService;
    @Autowired
    private CategoryService categoriesService;

    @RequestMapping({"", "/", "/dashboard"})
    public String root(Model model) {
        model.addAttribute("restaurants", Restaurant.values());
        model.addAttribute("activities", activityService.getAllActivities());
        model.addAttribute("categories", categoriesService.getALlCategories());
        model.addAttribute(new SimpleCategory());
        model.addAttribute(new SimpleActivity());
        
        SimpleFood simpleFood = new SimpleFood();
        simpleFood.setCategory(new SimpleCategory());
        model.addAttribute("simpleFood", simpleFood);
        
        return "admin_dashboard";
    }

}
