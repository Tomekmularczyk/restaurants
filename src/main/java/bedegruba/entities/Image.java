package bedegruba.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "images")
public class Image implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    
    @Column(nullable = false)
    private String name;
    
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Size(max = 524_000) //half MB
    @Column(nullable = false)
    private byte[] file;
    
    @OneToOne
    private Food food;

    public Image(){
    }
    
    public Image(String name, byte[] file) {
        this.name = name;
        this.file = file;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
    
    @Override
    public int hashCode() {
        int hash = 19;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Image other = (Image) obj;
        return this.id == other.id;
    }
    
    
}
