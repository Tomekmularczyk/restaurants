package bedegruba.entities.simple;

public class SimpleEmail {

    private String senderName;
    private String senderEmail;
    private String subject;
    private String message;

    public Builder builder() {
        return new Builder();
    }

    public SimpleEmail(){}
    
    public SimpleEmail(Builder builder) {
        this.senderName = builder.senderName;
        this.senderEmail = builder.senderEmail;
        this.subject = builder.subject;
        this.message = builder.message;
    }

    public static class Builder {

        private String subject;
        private String message;
        private String senderName;
        private String senderEmail;

        public Builder withSenderName(String senderName) {
            this.senderName = senderName;
            return this;
        }

        public Builder withSenderEmail(String senderEmail) {
            this.senderEmail = senderEmail;
            return this;
        }

        public Builder withSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder withMessage(String message) {
            this.senderEmail = message;
            return this;
        }

        public SimpleEmail build() {
            return  new SimpleEmail(this);
        }
    }

    public String getSenderName() {
        return senderName;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessage(String message) {
        this.message = message;
    }
   
}
