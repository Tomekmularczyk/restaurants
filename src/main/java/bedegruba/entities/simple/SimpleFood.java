package bedegruba.entities.simple;

import bedegruba.domain.Restaurant;
import bedegruba.entities.Food;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.web.multipart.MultipartFile;

public class SimpleFood {

    private Integer id;
    private Restaurant restaurant;
    private String name;
    private SimpleCategory category;
    private Date created;
    private Integer kcal;
    private Double carbohydrates;
    private Double proteins;
    private Double fats;
    private SimpleImage simpleImage;

    //w formularzu do zapisywania
    private MultipartFile multipartImage; 

    public static SimpleFood of(Food food) {
        SimpleFood simpleFood = new SimpleFood();
        simpleFood.setId(food.getId());
        simpleFood.setRestaurant(food.getRestaurant());
        simpleFood.setName(food.getName());
        simpleFood.setCategory(SimpleCategory.of(food.getCategory()));
        simpleFood.setKcal(food.getKcal());
        simpleFood.setFats(food.getFats());
        simpleFood.setProteins(food.getProteins());
        simpleFood.setCarbohydrates(food.getCarbohydrates());
        simpleFood.setCreated(food.getCreated());
        simpleFood.setSimpleImage(SimpleImage.of(food.getImage()));
        return simpleFood;
    }
    
    public static List<SimpleFood> of(List<Food> foods){
        return foods.stream()
                .map(SimpleFood::of)
                .collect(Collectors.toList());
    }

    public MultipartFile getMultipartImage() {
        return multipartImage;
    }

    public void setMultipartImage(MultipartFile image) {
        this.multipartImage = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SimpleCategory getCategory() {
        return category;
    }

    public void setCategory(SimpleCategory category) {
        this.category = category;
    }
    
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getKcal() {
        return kcal;
    }

    public void setKcal(Integer kcal) {
        this.kcal = kcal;
    }

    public Double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(Double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public Double getProteins() {
        return proteins;
    }

    public void setProteins(Double proteins) {
        this.proteins = proteins;
    }

    public Double getFats() {
        return fats;
    }

    public void setFats(Double fats) {
        this.fats = fats;
    }

    public SimpleImage getSimpleImage() {
        return simpleImage;
    }

    public void setSimpleImage(SimpleImage simpleImage) {
        this.simpleImage = simpleImage;
    }

}
