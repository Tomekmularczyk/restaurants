package bedegruba.entities.simple;

import bedegruba.entities.Image;
import bedegruba.services.StorageException;
import com.google.common.io.ByteStreams;
import java.io.IOException;
import java.io.InputStream;
import org.springframework.web.multipart.MultipartFile;

public class SimpleImage {

    private int imageId;
    private String name;
    private byte[] file;
    private int foodId;

    public static SimpleImage of(Image image) {
        SimpleImage simpleImage = new SimpleImage(image.getName(), image.getFile());
        simpleImage.setImageId(image.getId());
        simpleImage.setFoodId(image.getFood().getId());
        return simpleImage;
    }

    public static SimpleImage of(MultipartFile image) {
        String fileName = image.getName();
        try (InputStream inputStream = image.getInputStream()) {
            byte[] fileBytes = ByteStreams.toByteArray(inputStream);
            return new SimpleImage(fileName, fileBytes);
        } catch (IOException ex) {
            throw new StorageException("Could not save image for food", ex);
        }
    }
    
    public SimpleImage(String name, byte[] imageBytes){
        this.name = name;
        this.file = imageBytes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

}
