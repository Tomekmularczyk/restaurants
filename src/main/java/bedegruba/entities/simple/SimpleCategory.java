package bedegruba.entities.simple;

import bedegruba.domain.Restaurant;
import bedegruba.entities.Category;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleCategory implements Comparable<SimpleCategory>{
    private Integer id;
    private String name;
    private Restaurant restaurant;

    public SimpleCategory() {}
    
    public SimpleCategory(int id, String name, Restaurant restaurant) {
        this.id = id;
        this.name = name;
        this.restaurant = restaurant;
    }
    
    public static SimpleCategory of(Category category){
        return new SimpleCategory(
                category.getId(),
                category.getName(),
                category.getRestaurant()
        );
    }
    
    public static List<SimpleCategory> of(List<Category> categories) {
        return categories.stream()
                .map(SimpleCategory::of)
                .collect(Collectors.toList());
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public String toString() {
        return id + ":" + restaurant + ":" + name;
    }

    @Override
    public int compareTo(SimpleCategory another) {
        return another.getId().compareTo(this.id);
    }
}
