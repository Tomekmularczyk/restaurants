package bedegruba.entities.simple;

import bedegruba.entities.Activity;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleActivity {

    private Integer id;
    private String name;
    private int duration;

    public SimpleActivity(Integer id, String name, int duration) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }

    public static SimpleActivity of(Activity activity) {
        return new SimpleActivity(
                activity.getId(),
                activity.getName(),
                activity.getDuration()
        );
    }

    public SimpleActivity() {
    }

    public static List<SimpleActivity> of(List<Activity> activities) {
        return activities.stream()
                .map(SimpleActivity::of)
                .collect(Collectors.toList());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

}
