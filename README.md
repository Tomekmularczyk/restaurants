
### What is this repository for? ###

Web app. Calculates calories and macronutrients from composed KFC/MCDONALD/MCCAFE meals also suggesting activities required to burn those calories.

### Frontend:
Materialize CSS, Jquery, Gulp

### Backend:
Java, Spring, Hibernate (for keeping database of products) and exposing API for frontend.
