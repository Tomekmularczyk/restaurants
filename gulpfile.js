"use strict";

/**************************
 1. minifyScripts
 2. minifyCss
***************************/


const gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	babel = require("gulp-babel"),
	rename = require("gulp-rename"),
	cleanCss = require("gulp-clean-css");
	

gulp.task("indexMinify", function () {
	return gulp.src("src/main/resources/static/js/index.js")
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(rename("index.es5.min.js"))
        .pipe(gulp.dest("src/main/resources/static/js-dist"));
});

gulp.task("loginMinify", function () {
	return gulp.src("src/main/resources/static/js/login.js")
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(rename("login.es5.min.js"))
        .pipe(gulp.dest("src/main/resources/static/js-dist"));
});

gulp.task("admin-dashboardMinify", function () {  
    return gulp.src("src/main/resources/static/js/admin-dashboard.js")
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(rename("admin-dashboard.es5.min.js"))
    	.pipe(gulp.dest("src/main/resources/static/js-dist"));
});
	
gulp.task("indexScripts", function() {	
	return gulp.src([
		"src/main/resources/static/js-dist/libs/jquery-3.1.1.min.js", 
		"src/main/resources/static/js-dist/libs/materialize.min.js",
    	"src/main/resources/static/js-dist/libs/swiper.jquery.min.js",
		"src/main/resources/static/js-dist/index.es5.min.js"
	])
	.pipe(concat("index.es5.min.js"))
	.pipe(gulp.dest("src/main/resources/static/js-dist"));
});

gulp.task("loginScripts", function() {	
	return gulp.src([
		"src/main/resources/static/js-dist/libs/jquery-3.1.1.min.js", 
		"src/main/resources/static/js-dist/libs/materialize.min.js",
		"src/main/resources/static/js-dist/login.es5.min.js"
	])
	.pipe(concat("login.es5.min.js"))
	.pipe(gulp.dest("src/main/resources/static/js-dist"));
});

gulp.task("adminScripts", function() {
	return gulp.src([
		"src/main/resources/static/js-dist/libs/jquery-3.1.1.min.js", 
		"src/main/resources/static/js-dist/libs/materialize.min.js",
		"src/main/resources/static/js-dist/admin-dashboard.es5.min.js"
	])
	.pipe(concat("admin-dashboard.es5.min.js"))
	.pipe(gulp.dest("src/main/resources/static/js-dist"));
});

gulp.task("toEs5Minify", ["indexMinify", "loginMinify", "admin-dashboardMinify"], function () {
	console.log("index.js, login.js, admin-dashboard.js were minified in folder js-dist.");
});

gulp.task("concatScripts", ["loginScripts", "adminScripts", "indexScripts"], function (){
	console.log("index, login and admin page scripts were concatenated");
});


gulp.task("minifyScripts", ["toEs5Minify"], function () {
	gulp.start("concatScripts");
});


/*************************************************************
* C S S
************************************************************/

gulp.task("minifyCss", function () {
	gulp.src(["src/main/resources/static/css/login.css"])
		.pipe(cleanCss())
		.pipe(rename("login.min.css"))
		.pipe(gulp.dest("src/main/resources/static/css-dist"));
		
	gulp.src(["src/main/resources/static/css/index.css"])
		.pipe(cleanCss())
		.pipe(rename("index.min.css"))
		.pipe(gulp.dest("src/main/resources/static/css-dist"));
		
	gulp.src(["src/main/resources/static/css/admin-dashboard.css"])
		.pipe(cleanCss())
		.pipe(rename("admin-dashboard.min.css"))
		.pipe(gulp.dest("src/main/resources/static/css-dist"));
	
	console.log("css files were minified to folder /static/css-dist");
});



gulp.task("default", ["minifyScripts", "minifyCss"], function () {
	console.log("FINISHED");
});

